DEPENDÊNCIAS:

MongoDB 3.6
https://www.mongodb.com/download-center/community

Python 3.x
https://www.python.org/downloads/

blast-dbf
https://github.com/eaglebh/blast-dbf
Uma clone do repositório de 15/07/19 está no arquivo blast-dbf-master.zip.

dbf2csv
https://github.com/akadan47/dbf2csv
Uma clone do repositório de 15/07/19 está no arquivo dbf2csv-master.zip.

Uso:
É passado como parâmetro um arquivo de entrada com as especificações do ETL. Os arquivos input.json, sih.json e sinasc.json são exemplos de uso.

Para execução do ETL:
$etl arquivo_de_entrada